﻿using System;
using Jq.Model;
using Jq.Respository;
using Jq.Service.Interface;

namespace Jq.Service
{
    public class User : IUser
    {
        private IDataAccessProvider<Jq.Domain.User> _provider;

        public User(IDataAccessProvider<Jq.Domain.User> provider)
        {
            _provider = provider;
            //_provider = Services.GetRequiredService<IDataAccessProvider<Jq.Domain.User>>();
        }

        public int AddUser(Jq.Model.M_User mUser)
        {
            Jq.Domain.User user = new Domain.User() {
                Name = mUser.Name,
                EnName = mUser.EnName
            };
            var res =  _provider.Add(user);

            return res;
        }
    }
}
