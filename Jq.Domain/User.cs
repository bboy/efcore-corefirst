﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jq.Domain
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string EnName { get; set; }
        public string JpName { get; set; }
        public int? Age { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}
