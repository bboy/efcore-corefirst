﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Jq.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;

namespace Jq.Respository
{
    public class DataAccessMsSqlServerProvider<T> : IDataAccessProvider<T> where T : class
    {
        private readonly DomainModelMsSqlServerContext _context;
        private readonly ILogger _logger;

        public DataAccessMsSqlServerProvider(DomainModelMsSqlServerContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessMsSqlServerProvider");
        }

        public int Add(T model)
        {
            _context.Set<T>().Add(model);
            return _context.SaveChanges();
        }

        public bool Delete(int id)
        {
            var model = _context.Set<T>().Find(id);
            _context.Set<T>().Remove(model);

            return _context.SaveChanges() > 0;
        }

        public int Edit(T model)
        {
            EntityEntry entry = _context.Entry<T>(model);
            entry.State = EntityState.Modified;

            var res = _context.SaveChanges();

            return res;
        }
    }
}
