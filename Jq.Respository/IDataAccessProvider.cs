﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Jq.Domain;

namespace Jq.Respository
{
    public interface IDataAccessProvider<T>
    {
        int Add(T model);

        int Edit(T model);

        bool Delete(int id);
    }
}
