﻿using System;

namespace Jq.Model
{
    public class M_User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EnName { get; set; }
        public int? Age { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}
