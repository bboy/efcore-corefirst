﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jq.Respository;
using Jq.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUser _userService;

        public UserController(IUser userService)
        {
            //var provider1 = this.Request.HttpContext.ApplicationServices; //当前应用程序里注册的Service
            //var provider2 = Context.RequestServices;  // Controller中，当前请求作用域内注册的Service
            //var provider3 = Resolver; //Controller中

            //var provider = this.Request.HttpContext.RequestServices;
            //var user = provider.GetService(typeof(IDataAccessProvider<Jq.Domain.User>));

            _userService = userService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            Jq.Model.M_User mUser = new Jq.Model.M_User();
            mUser.Name = "ef";
            mUser.Age = 2;

            _userService.AddUser(mUser);

            return Created("/api/user", mUser);

            //return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
